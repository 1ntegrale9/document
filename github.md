## TIPS

#### keyword
Git LFS

## コミットの差分を見る

#### discription
```
https://github.com/arg1/arg2/compare/arg3...arg4

arg1 = account_id 
arg2 = repository_name  
arg3 = commit_hash or branch_name  
arg4 = commit_hash or branch_name  
```
- arg3からarg4までの差分を表示する。
- arg3時点の変更は表示されず、arg4時点の変更は表示される。

#### example
- https://github.com/1ntegrale9/config/compare/383bf80...master

#### refs
github でブランチ・commit間の diff を見る
http://qiita.com/fantasista_21jp/items/9419ca4ab3bb8e1ee4c5

#### search
- github コミット diff
- github コミット 差分
