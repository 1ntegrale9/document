## レスポンシブデザイン

#### keyword
bootstrap, viewport

## CSS設計

#### keyword
OOCSS, SMACSS, BEM, SuitCSS, FLOCSS

## FLOCSS

#### keyword
Foundation, Layout, Object, Component, Project, Utility

#### discription
接頭辞c-,p-,u-が付くのが特徴。
componentとutilityは意味が分かりやすいが、projectは何のことだか分かりにくい。
project内にcomponentが存在する？

https://github.com/hiloki/flocss
>プロジェクト固有のパターンであり、いくつかのComponentと、それに該当しない要素によって構成されるもの

>記事一覧や、ユーザープロフィール、画像ギャラリーなどコンテンツを構成する要素など

#### refs
【CSS設計】FLOCSSを導入してみて気づいた８つのポイント
https://www.indival.co.jp/2016/08/26/3102/

Projectレイヤーを使いこなす！ – FLOCSSで始めるCSS設計
https://www.to-r.net/media/floccs-04/
