## TIPS

#### refs

JavaScriptを読んでて「なにこれ！？」と思うけれど調べられない記法8選。
http://qiita.com/ukiuni@github/items/5f3d8620187905aea3d4

## jQuery

#### refs

jQueryのセレクタメモ
http://qiita.com/Thought_Nibbler/items/5d4fc40a4d4325128b24

jQueryで親要素を取得する：parent(), parents(), closest()
http://uxmilk.jp/8150